'''
FilePath: \playwright-demo-master\Pages\MyAccountPage\MyAccountPage.py
Author: Struggling youth
Date: 2023-06-09 16:07:56
LastEditTime: 2023-06-09 16:25:46
Description: 我的账号页面

Copyright (c) 2023 by Struggling youth, All Rights Reserved. 
'''

import allure

from BasePage.BasePage import BasePage

class MyAccountPage(BasePage):
    # 定位器
    __logout = 'a[title = "退出"]'

    @allure.step("断言安全退出可见")
    def logout_to_be_visible(self,locator):
        return self._ele_to_be_visible(locator)




