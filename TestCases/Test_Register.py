'''
FilePath: \playwright-demo-master\TestCases\Test_Register.py
Author: Struggling youth
Date: 2023-06-09 16:07:56
LastEditTime: 2023-06-09 16:30:14
Description: 注册用例

Copyright (c) 2023 by Struggling youth, All Rights Reserved. 
'''

import time
import allure, os
import pytest
from Pages.LoginPage.LoginPage import LoginPage
from Pages.RegisterPage.RegisterPage import RegisterPage
from Pages.MyAccountPage.MyAccountPage import MyAccountPage
from Utils.ReadYaml import ReadYaml
from Common.AllurePretty import PrettyAllure
from Config.Config import Config


class TestRegister:

    @pytest.mark.smoking
    @pytest.mark.run(order=0)
    @PrettyAllure.PrettyAllureWarpper
    @pytest.mark.parametrize("CaseData", ReadYaml(os.path.join(Config.test_datas_dir, "TestRegisterData.yaml")).read())
    def test_register(self, page, CaseData: dict):
        rp = RegisterPage(page)
        rp.del_auth()
        rp.goto_register(url=CaseData["url地址"])
        rp.type_username(username=CaseData["用户名"])
        rp.type_verify_code(CaseData["验证码"])
        rp.type_password(CaseData["密码"])
        rp.type_password2(CaseData["确认密码"])
        rp.click_checktxt()
        rp.click_btn_agree()
        MyAccountPage(page).logout_to_be_visible(CaseData["断言元素定位"])
        rp.click_ele(CaseData["断言元素定位"])
